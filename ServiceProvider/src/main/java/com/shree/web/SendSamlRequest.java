package com.shree.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shree.web.samlrequest.signin.CreateSamlSignInRequest;

@SuppressWarnings("serial")
@WebServlet("/samlRequest")
public class SendSamlRequest extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		CreateSamlSignInRequest createSamlRequestObj = new CreateSamlSignInRequest();

		response.setContentType("text/html");

		if (request.getParameter("signin") != null) {

			// Check whether already signed in
			IsSessionActive obj = new IsSessionActive();
			if (obj.getSessionStatus()) {
				response.getWriter().print("<h2>User session exists</h2>"
						+ "<h4>Redirected to home page in 3 sec</h4>"
						+ "<h2>SAML Request is not sent again.</h2>");
				response.setHeader("Refresh", "3; URL=http://localhost:8080/ServiceProvider/welcome");
			} else {
				try {
					// Creating SAML request
					createSamlRequestObj.createXmlReq();

					// Fetching the URL as string
					String url = createSamlRequestObj.getRequestUrl();

					if (url.equals("")) {
						response.getWriter().print("Issue in creating SAML Request");
						response.setHeader("Refresh", "3; URL=http://localhost:8080/ServiceProvider/LoginPage");
					}
					
					response.sendRedirect(url);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		doGet(request, response);
	}
}
