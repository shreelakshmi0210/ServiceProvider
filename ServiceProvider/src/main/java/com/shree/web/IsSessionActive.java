package com.shree.web;

// To check whether the user is signed in or not
 class IsSessionActive {
	
	// Should remove static as because there will be multiple users
	private static boolean sessionStatus = false;

	public boolean getSessionStatus() {
		return sessionStatus;
	}
	
	public void setActive() {
		sessionStatus = true;
	}
	
	public void setInactive() {
		sessionStatus = false;
	}

}
