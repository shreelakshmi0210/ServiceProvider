package com.shree.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shree.web.db.Database;
import com.shree.web.samlrequest.logout.CreateSamlLogoutRequest;

@WebServlet("/logoutRequest")
public class SendLogoutRequest extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");

		HttpSession session = request.getSession(false);

		if (session == null) {
			response.getWriter().print("<h2>You have been logged out</h2>" 
					+ "<h2>Please login again</h2>"
					+ "<form action=\"http://localhost:8080/ServiceProvider/LoginPage\" method=\"post\">"
					+ "<input type=\"submit\" name=\"signin\" value=\"Sign in\" />"
					+ "</form>");
			return;
		}

		String userId = (String) session.getAttribute("userId");
		Database dbObject = Database.getInstance();
		String ssoId = dbObject.getSsoRequestId(userId);

		System.out.println(userId + " " + ssoId);

		CreateSamlLogoutRequest logoutRequestObj = new CreateSamlLogoutRequest();

		try {
			logoutRequestObj.createXmlReq(userId, ssoId);

			String url = logoutRequestObj.getRequestUrl();

			if (url.equals("")) {
				response.getWriter().print("Issue in creating Logout Request");
				return;
			}

			response.sendRedirect(url);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
