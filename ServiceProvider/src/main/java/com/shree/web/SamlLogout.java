package com.shree.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shree.web.db.Database;

@SuppressWarnings("serial")
@WebServlet("/logout")
public class SamlLogout extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		
		HttpSession session = request.getSession(false);
		session.invalidate();
		
		IsSessionActive obj = new IsSessionActive();
		obj.setInactive();

		try {
			if (!(request.getParameter("SAMLResponse").equals(null))) {
				// store the response for reference
				String responseString = request.getParameter("SAMLResponse");
				StoreSamlResponse storeSamlResponseObj = new StoreSamlResponse();
				storeSamlResponseObj.storeSamlResponse(responseString);
			} 
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			obj.setActive();
		}
		
		Database dbObject = Database.getInstance();
		dbObject.removeUserDetails(dbObject.getTempUserId());
		
		response.getWriter().print("<title>Logged out</title>");
		response.getWriter().print("<link rel=\"stylesheet\" href=\"style.css\">");
		response.getWriter().print("<h2>Successfully consumed the Logout Response</h2>"
				+ "<div class=\"log-form\">\n"
				+ "		<form action=\"http://localhost:8080/ServiceProvider/LoginPage\" method=\"post\">\n"
				+ "			<input type=\"submit\" name=\"signin\" value=\"Back to login page\"/>\n"
				+ "		</form>\n"
				+ "	</div>");

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	
}
