package com.shree.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginPage
 */
@WebServlet("/LoginPage")
public class LoginPage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		response.getWriter().print("<title>Login</title>");
		response.getWriter().print("<link rel=\"stylesheet\" href=\"style.css\">");
		response.getWriter().print("<div class=\"log-form\">\n"
				+ "		<h2>Login to your account</h2>\n"
				+ "		<form action=\"http://localhost:8080/ServiceProvider/samlRequest\" method=\"post\">\n"
				+ "			<input type=\"text\" title=\"username\" placeholder=\"username\" /> \n"
				+ "			<input type=\"password\" title=\"username\" placeholder=\"password\" /> \n"
				+ "			<input type=\"submit\" name=\"btn\" value=\"Sign in\" /> 	\n"
				+ "			<input type=\"submit\" name=\"signin\" value=\"Sign in using Okta\"/>\n"
				+ "		</form>\n"
				+ "	</div>");
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
