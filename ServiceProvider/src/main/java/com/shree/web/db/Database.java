package com.shree.web.db;

import java.util.HashMap;
import java.util.Map;

public final class Database {
	private static Database dbObject = null;
	private static Map<String,String> sessionDetails =  new HashMap<String, String>();
	private static String tempSsoRequestId;
	private static String tempUserId;
	
	public static Database getInstance() {
		if (dbObject == null)
				dbObject = new Database();
		return dbObject;
	}
	
	public void storeUserLoginDetails(String userId, String ssoRequestId) {
		sessionDetails.put(userId, ssoRequestId);	
	}
	
	public void storeTempSsoRequestId(String ssoRequestId) {
		tempSsoRequestId = ssoRequestId;
	}
	
	public String getTempSsoRequestId() {
		return tempSsoRequestId;
	}
	
	public String getSsoRequestId(String userId) {
		return sessionDetails.get(userId);
	}
	
	public void storeTempUserId(String userId) {
		tempUserId = userId;
	}
	
	public String getTempUserId() {
		return tempUserId;
	}
	
	public void removeUserDetails(String userID) {
		sessionDetails.remove(userID);
	}

}
