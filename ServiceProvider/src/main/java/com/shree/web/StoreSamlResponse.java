package com.shree.web;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.xml.security.encryption.XMLCipher;
import org.apache.xml.security.utils.EncryptionConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.shree.web.db.Database;

public class StoreSamlResponse {

	public void storeSamlResponse(String responseString) {

		byte[] base64DecodedResponse = Base64.getDecoder().decode(responseString);
		String decodedResponse = new String(base64DecodedResponse, StandardCharsets.UTF_8);

		if (decodedResponse.contains("saml2p:LogoutResponse")) {
			try {
				Path path = Paths.get("/home/local/ZOHOCORP/shree-pt6203/workspace/shree-projects/ServiceProvider/src/main/resources/test/LogoutResponse.xml");
				Files.writeString(path, decodedResponse, StandardCharsets.UTF_8);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}

		// Storing the encoded response in a XML file
		try {

			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(decodedResponse)));

			// Decrypting the encrypted response
//			doc  = decryptResponse(doc);

			// Write the parsed document to an XML file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);

			StreamResult result =  new StreamResult(new File("/home/local/ZOHOCORP/shree-pt6203/workspace/shree-projects/ServiceProvider/src/main/resources/test/SignInResponse.xml"));
			transformer.transform(source, result);

			//  Storing user details
			String userId = extractUserId(doc);
			Database dbObject = Database.getInstance();
			String ssoRequestId = dbObject.getTempSsoRequestId();
			dbObject.storeUserLoginDetails(userId, ssoRequestId);
			dbObject.storeTempUserId(userId);

		} catch (Exception e) {
			e.printStackTrace();
		}	

	}

	private String extractUserId(Document doc) {
		NodeList nodeList = doc.getElementsByTagName("saml2:Attribute");
		Element userId = (Element) nodeList.item(3);
		String userIdValue = userId.getTextContent();
		return userIdValue;
	}

	private Document decryptResponse(Document document) {

		try {
			Path fileName = Path.of("/home/local/ZOHOCORP/shree-pt6203/workspace/shree-projects/ServiceProvider/src/main/resources/credentials/private.key");
			byte[] key = (Files.readString(fileName)).getBytes();
			String algorithm = "AES";
			SecretKey secretKey = new SecretKeySpec(key, 0, key.length, algorithm) ;

			Element encryptedDataElement = (Element) document.getElementsByTagNameNS(EncryptionConstants.EncryptionSpecNS, EncryptionConstants._TAG_ENCRYPTEDDATA).item(0);

			XMLCipher xmlCipher = XMLCipher.getInstance();

			xmlCipher.init(XMLCipher.DECRYPT_MODE, secretKey);
			xmlCipher.doFinal(document, encryptedDataElement);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return document;

	}

}
