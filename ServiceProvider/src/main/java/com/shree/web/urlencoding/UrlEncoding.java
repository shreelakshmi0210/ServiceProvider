package com.shree.web.urlencoding;

import java.io.ByteArrayOutputStream;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.util.Base64;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;

public class UrlEncoding {

	String encodedUrl;

	public String getEncodedUrl(Document doc) throws Exception {
		String xmlString;
		xmlString = writeXmlAndReturnXmlString(doc);
		String encodedUrl = encryptDeflateAndBase64(xmlString);

		return encodedUrl;
	}

	public String writeXmlAndReturnXmlString(Document doc) throws TransformerException {

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();

		// Pretty print XML
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(
				"/home/local/ZOHOCORP/shree-pt6203/workspace/shree-projects/ServiceProvider/src/main/resources/test/LogoutRequest.xml");

		transformer.transform(source, result);

		// Sign the request XML file
//		GenerateXMLDigitalSignature.signRequestXml();

		StringWriter writer = new StringWriter();
		transformer.transform(new DOMSource(doc), new StreamResult(writer));

		return writer.toString();

	}

	public static String encryptDeflateAndBase64(String xmlData) throws Exception {

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Deflater deflater = new Deflater(Deflater.DEFAULT_COMPRESSION, true);
		DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(os, deflater);
		deflaterOutputStream.write(xmlData.getBytes("UTF-8"));
		deflaterOutputStream.close();
		os.close();
		String base64 = new String(Base64.getEncoder().encode(os.toByteArray()));

		return URLEncoder.encode(base64, "UTF-8");

	}

}
