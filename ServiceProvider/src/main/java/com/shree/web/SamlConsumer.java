package com.shree.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shree.web.db.Database;

@SuppressWarnings("serial")
@WebServlet("/welcome")
public class SamlConsumer extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");

		IsSessionActive obj = new IsSessionActive();

		try {
			if (!(request.getParameter("SAMLResponse").equals(null))) {
				// store the response for reference
				String responseString = request.getParameter("SAMLResponse");
				StoreSamlResponse storeSamlResponseObj = new StoreSamlResponse();
				storeSamlResponseObj.storeSamlResponse(responseString);
			} 
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			obj.setActive();
		}

		Database dbObject = Database.getInstance();
		String userId = dbObject.getTempUserId();

		// Session management: HttpSession 
		HttpSession session = request.getSession();
		session.setAttribute("userId", userId);

		response.getWriter().print("<h2 text-align=\"center\">Welcome, " + userId + "</h2>");
		response.getWriter().print("<h2 text-align=\"center\">Successfully consumed the SAML Assertion</h2>");

		// Adding SLO option
		response.getWriter().println(
				"<form action=\"http://localhost:8080/ServiceProvider/logoutRequest\" method=\"post\">"
						+ "<input type=\"submit\" name=\"logout\" value=\"Single Log Out\" />"
						+ "</form>");

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
