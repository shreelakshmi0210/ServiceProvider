package com.shree.web.samlrequest.logout;

import java.util.HashMap;
import java.util.UUID;

public class LogoutRequestParameters {

	@SuppressWarnings("serial")
	private HashMap<String, String> attributes = new HashMap<String, String>() {
		{
			put("Destination",
					"https://dev-92809401.okta.com/app/dev-92809401_samlserviceprovider1_1/exk7tl1awwyuzWlbw5d7/slo/saml");
			put("ID", UUID.randomUUID().toString());
			put("IssueInstant", java.time.LocalDateTime.now().toString());
			put("NotOnOrAfter", java.time.LocalDateTime.now().plusMinutes(30).toString());
			put("Reason", "urn:oasis:names:tc:SAML:2.0:logout:user");
			put("Version", "2.0");
			put("xmlns:saml", "urn:oasis:names:tc:SAML:2.0:assertion");
			put("xmlns:samlp", "urn:oasis:names:tc:SAML:2.0:protocol");
		}
	};

	private String issuerElementTextContentSPEntityID = "http://localhost:8080/saml-service-provider-1";
	private String nameIdFormatElement = "urn:oasis:names:tc:SAML:1.1:name-id-format:unspecified";

	public HashMap<String, String> getLogoutRequestParameters() {
		return attributes;
	}

	public String getissuerElementTextContentSPEntityID() {
		return issuerElementTextContentSPEntityID;
	}
	
	public String getNameIdFormatElement() {
		return nameIdFormatElement;
	}
}
