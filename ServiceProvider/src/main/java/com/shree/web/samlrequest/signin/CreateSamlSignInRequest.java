package com.shree.web.samlrequest.signin;

import java.io.File;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.shree.web.db.Database;
import com.shree.web.urlencoding.UrlEncoding;

public class CreateSamlSignInRequest {

	final String IDP_SSO_URL = "https://dev-92809401.okta.com/app/dev-92809401_samlserviceprovider1_1/exk7tl1awwyuzWlbw5d7/sso/saml?SamlRequest=";
	private static String samlRequestUrlEncoded = "";
	private static String completeUrl = "";

	public void createXmlReq() throws Exception {

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Document doc = docBuilder.newDocument();

		// Setting header AuthnRequest
		Element authnRequest = doc.createElement("saml2p:AuthnRequest");
		AuthnRequestParameters authnRequestParametersObj = new AuthnRequestParameters();
		HashMap<String, String> attributes = authnRequestParametersObj.getAuthnRequestHeaderAttributes();

		for (String key : attributes.keySet()) {
			authnRequest.setAttribute(key, attributes.get(key));
		}

		// Setting SAML issuer
		Element issuerElement = doc.createElement("saml2:Issuer");
		issuerElement.setAttribute("xmlns:saml2", authnRequestParametersObj.getIssuerElement());
		issuerElement.setTextContent(authnRequestParametersObj.getissuerElementTextContentSPEntityID());

		authnRequest.appendChild(issuerElement);

		doc.appendChild(authnRequest);

		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);

		StreamResult result =  new StreamResult(new File("/home/local/ZOHOCORP/shree-pt6203/workspace/shree-projects/ServiceProvider/src/main/resources/test/SignInRequest.xml"));
		transformer.transform(source, result);
		
		Database dbObject = Database.getInstance();
		dbObject.storeTempSsoRequestId(authnRequestParametersObj.getSsoRequestId());

		// Getting the request as encoded URL
		UrlEncoding encodingObj = new UrlEncoding();
		samlRequestUrlEncoded = encodingObj.getEncodedUrl(doc);

		completeUrl = IDP_SSO_URL + samlRequestUrlEncoded;

	}

	public String getRequestUrl() {
		return completeUrl;
	}

}
