package com.shree.web.samlrequest.logout;

import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.shree.web.urlencoding.UrlEncoding;

public class CreateSamlLogoutRequest {

	final String IDP_SLO_URL = "https://dev-92809401.okta.com/app/dev-92809401_samlserviceprovider1_1/exk7tl1awwyuzWlbw5d7/slo/saml?SAMLRequest=";
	private static String samlRequestUrlEncoded = "";
	private static String completeUrl = "";

	public void createXmlReq(String userId, String ssoId) throws Exception {

		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		Document doc = docBuilder.newDocument();

		// Setting header AuthnRequest
		Element logoutRequest = doc.createElement("samlp:LogoutRequest");
		LogoutRequestParameters logoutRequestParametersObj = new LogoutRequestParameters();
		HashMap<String, String> attributes = logoutRequestParametersObj.getLogoutRequestParameters();

		for (String key : attributes.keySet()) {
			logoutRequest.setAttribute(key, attributes.get(key));
		}

		// Setting SAML issuer
		Element issuerElement = doc.createElement("saml:Issuer");
		issuerElement.setTextContent(logoutRequestParametersObj.getissuerElementTextContentSPEntityID());
		logoutRequest.appendChild(issuerElement);

		// Setting NameID
		Element nameID = doc.createElement("saml:NameID");
		nameID.setAttribute("Format", logoutRequestParametersObj.getNameIdFormatElement());
		nameID.setTextContent(userId);
		logoutRequest.appendChild(nameID);

		// Setting session index
		Element sessionIndex = doc.createElement("saml:SessionIndex");
		sessionIndex.setTextContent(ssoId);
		logoutRequest.appendChild(sessionIndex);

		doc.appendChild(logoutRequest);
		
		// Getting the request as encoded URL
		UrlEncoding encodingObj = new UrlEncoding();
		samlRequestUrlEncoded = encodingObj.getEncodedUrl(doc);
		
		completeUrl = IDP_SLO_URL + samlRequestUrlEncoded;

	}

	public String getRequestUrl() {
		return completeUrl;
	}

}
