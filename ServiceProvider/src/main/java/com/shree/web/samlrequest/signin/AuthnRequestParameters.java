package com.shree.web.samlrequest.signin;

import java.util.HashMap;
import java.util.UUID;

public class AuthnRequestParameters {
	
	private String uniqueSsoRequestId;	
    @SuppressWarnings("serial")
	private HashMap<String, String> attributes = new HashMap<String, String>()
			{{
				put("xmlns:saml2p", "urn:oasis:names:tc:SAML:2.0:protocol");
				put("AssertionConsumerServiceURL", "http://localhost:8080/ServiceProvider/welcome");
				put("Destination", "https://dev-92809401.okta.com/app/dev-92809401_samlserviceprovider1_1/exk7tl1awwyuzWlbw5d7/sso/saml");
				put("ID", uniqueSsoRequestId);
				put("IssueInstant", java.time.LocalDateTime.now().toString());
				put("ProtocolBinding", "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST");
				put("Version", "2.0");
			}};
		private String issuerElement = "urn:oasis:names:tc:SAML:2.0:assertion";
		private String issuerElementTextContentSPEntityID = "http://localhost:8080/saml-service-provider-1";
		
	public AuthnRequestParameters() {
		uniqueSsoRequestId = UUID.randomUUID().toString();
	}
	
	public HashMap<String, String> getAuthnRequestHeaderAttributes() {
		return attributes;
	}

	public String getIssuerElement() {
		return issuerElement;
	}

	public String getissuerElementTextContentSPEntityID() {
		return issuerElementTextContentSPEntityID;
	}
	
	public String getSsoRequestId() {
		return uniqueSsoRequestId;
	}
}
